# TelegrammBot for send ETH distribution


## Reproducibility 📻



### Prepare envroment


```
pip3 install web3 pyTelegramBotAPI
```

### Run script


```
python3 main.py \
 --tgapi  <<add API key>> \
 --contract-address <<ETHcontract-address>> \
 --infuraid  <<infura_project_id from https://app.infura.io/>> \
 --abi <<path of ABI json file from https://etherscan.io/>>

```


